import sys
from alembic.config import main as al_main
from alembic.config import CommandLine as AlCommandLine
from argparse import ArgumentParser


class CommandLine:
    def __init__(self, argv, prog):
        self.argv = argv
        self.prog = prog
        parser = ArgumentParser(prog=self.prog)
        subparsers = parser.add_subparsers()
        subparser = subparsers.add_parser('init', help='Initialize')
        subparser.add_argument('directory', type=str, help='location of config file')
        subparser.add_argument('-n', '--name', type=str, help='name of config file')
        subparsers.add_parser('up', help='help')
        subparsers.add_parser('alembic', help='help')
        self.parser = parser

    def parse_args(self):
        if 'alembic' in self.argv[0]:
            al_command = AlCommandLine(prog='vagabond')
            al_command.main(self.argv[1:])
        else:
            self.parser.parse_args(self.argv)


def main(argv=None):
    if argv is None:
        argv = sys.argv

    argv.pop(0)
    command = CommandLine(argv, prog='vagabond')
    command.parse_args()
    # argv.pop(0)
    # al_main(argv=argv, prog='Vagabond')


if __name__ == '__main__':
    main()
