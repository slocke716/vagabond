from vagabond import DatabaseObject
from vagabond.tests.samples.Objects.Functions import test_func


class DbObject(DatabaseObject):
    UPGRADE = """
    create or replace function test_func2(integer) RETURNS integer
    AS 'select public.test_func($1 + 20);'
    LANGUAGE SQL
    IMMUTABLE
    RETURNS NULL ON NULL INPUT;
    """

    DOWNGRADE = """
    drop function if exists public.test_func2(integer)
    """

    DEPENDS = [test_func]
