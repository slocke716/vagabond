from vagabond import DatabaseObject
from vagabond.tests.samples.Objects.Views import test_view


class DbObject(DatabaseObject):
    UPGRADE = """
    create or replace function public.test_func3(i integer) returns integer AS $$
    declare test int;
    begin
      select tst into test from public.test_view limit 1;
      return i + test;
    end;
    $$ language plpgsql;
    """

    DOWNGRADE = """
    drop function if exists public.test_func3(integer)
    """

    DEPENDS = [test_view]
