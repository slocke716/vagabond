from vagabond.tests.samples.Objects.Functions import test_func
from vagabond.tests.samples.Objects.Functions import test_func3
from vagabond.tests.samples.Objects.Views import test_view
from vagabond.tests.samples.Objects.Views import test_view2

from vagabond import DatabaseObject
from vagabond.tests.samples.Objects.Functions import test_func2


class DbObject(DatabaseObject):
    UPGRADE = """
    create materialized view if not exists public.test_matview as
    select
        public.test_func(t.tst) as tf1,
        public.test_func2(tt.tst) as tf2,
        public.test_func3(t.tst) as tf3
    from public.test_view t
    inner join public.test_view2 tt
        on 1=1
    """

    DOWNGRADE = """
    drop materialized view if exists public.test_matview
    """

    DEPENDS = [test_func, test_func2, test_func3, test_view, test_view2]

    @classmethod
    def walk_depends(cls):
        return super().walk_depends()
